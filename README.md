# Docker Toolbox

Toolbox Container


## Features ##

- MySQL Client


## Exposed volumes ##

None.


## Exposed ports ##

- 22 / SSH


## Environment Variables

None.


## Usage ##

Link to other containers and use the tools.

use with ssh - `docker run docker run -d --name="toolbox" -p 20022:22 thstangenberg/toolbox` 
use with shell - `docker run docker run -d --name="toolbox" thstangenberg/toolbox /sbin/my_init -- bash -l` 

## License ##

[Published under the MIT License][LICENSE]


[LICENSE]: https://bitbucket.org/thstangenberg/docker-baseimage/src/master/LICENSE.md "Published under the MIT License"